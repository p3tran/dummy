package com.vivantehealth.akazoocodingschool;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class PlaylistsActivity extends AppCompatActivity implements PlaylistsView{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlists);
        mRecyclerView = findViewById(R.id.playlists_rv);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ArrayList<Playlist> myDataset = new ArrayList<>();

        Playlist playlist = new Playlist("id", "name", "66", "http://www.akazoo.com/MusicStore/images/3/playlists/bf7dc3b1-7596-4720-816d-7fdc5e52676a/150/635162158182770000/cover.jpg");
        myDataset.add(playlist);myDataset.add(playlist);
        myDataset.add(playlist);myDataset.add(playlist);
        myDataset.add(playlist);myDataset.add(playlist);
        myDataset.add(playlist);myDataset.add(playlist);
        myDataset.add(playlist);myDataset.add(playlist);
        myDataset.add(playlist);myDataset.add(playlist);
        myDataset.add(playlist);myDataset.add(playlist);
        myDataset.add(playlist);myDataset.add(playlist);
        myDataset.add(playlist);myDataset.add(playlist);




        mAdapter = new PlaylistsRvAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
    }

}
