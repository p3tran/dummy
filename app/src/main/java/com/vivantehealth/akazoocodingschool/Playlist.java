package com.vivantehealth.akazoocodingschool;

public class Playlist {

    private String PlaylistId;
    private String Name;
    private String ItemCount;
    private String PhotoUrl;

    public Playlist() {
    }

    public Playlist(String playlistId, String name, String itemCount, String photoUrl) {
        PlaylistId = playlistId;
        Name = name;
        ItemCount = itemCount;
        PhotoUrl = photoUrl;
    }

    public String getPlaylistId() {
        return PlaylistId;
    }

    public void setPlaylistId(String playlistId) {
        PlaylistId = playlistId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getItemCount() {
        return ItemCount;
    }

    public void setItemCount(String itemCount) {
        ItemCount = itemCount;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }
}
