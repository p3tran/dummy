package com.vivantehealth.akazoocodingschool;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PlaylistsRvAdapter extends RecyclerView.Adapter<PlaylistsRvAdapter.MyViewHolder> {

    private ArrayList<Playlist> mPlaylists;

    public PlaylistsRvAdapter(ArrayList<Playlist> mPlaylists) {
        this.mPlaylists = mPlaylists;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView mPlaylistName;
        public ImageView mPlaylistImage;
        public TextView mTracksNumber;

        public MyViewHolder(View v) {
            super(v);
            mPlaylistName = v.findViewById(R.id.playlist_name);
            mPlaylistImage = v.findViewById(R.id.playlist_image);
            mTracksNumber = v.findViewById(R.id.track_number);
        }
    }

    @Override
    public PlaylistsRvAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_playlist_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.mPlaylistName.setText(mPlaylists.get(position).getName());
        holder.mTracksNumber.setText(mPlaylists.get(position).getItemCount());
        Picasso.get().load(mPlaylists.get(position).getPhotoUrl()).into(holder.mPlaylistImage);
    }

    @Override
    public int getItemCount() {
        return mPlaylists.size();
    }
}
